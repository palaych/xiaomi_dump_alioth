# qssi-user 11 RKQ1.200826.002 V12.0.5.0.RKHEUXM release-keys
- manufacturer: xiaomi
- platform: kona
- codename: alioth
- flavor: qssi-user
- release: 11
- id: RKQ1.200826.002
- incremental: V12.0.5.0.RKHEUXM
- tags: release-keys
- fingerprint: Redmi/alioth_eea/alioth:11/RKQ1.200826.002/V12.0.5.0.RKHEUXM:user/release-keys
POCO/alioth_eea/alioth:11/RKQ1.200826.002/V12.0.5.0.RKHEUXM:user/release-keys
Mi/aliothin/aliothin:11/RKQ1.200826.002/V12.0.5.0.RKHEUXM:user/release-keys
- is_ab: true
- brand: Redmi
- branch: qssi-user-11-RKQ1.200826.002-V12.0.5.0.RKHEUXM-release-keys
- repo: redmi_alioth_dump
